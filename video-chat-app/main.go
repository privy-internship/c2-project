package main

import (
	"log"
	"net/http"
	"video-chat-app/server"
)

func main() {

	// Inisialisasikan data
	server.AllRooms.Init()

	http.HandleFunc("/create", server.CreateRoomRequestHandler)
	http.HandleFunc("/join", server.JoinRoomReqHand)

	log.Println("Starting server on Post 8000")
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal(err)
	}
}
