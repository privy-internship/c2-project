const CreateRoom = (props) => {
    const create = async (e) => {
        e.preventDefault();

        const resp = await fetch("http://localhost:8000/create");
        const { room_id } = await resp.json();

		let p = props.history.push(`/room/${room_id}`)

        console.log(p)
    };

    return (
        <div>
            <button onClick={create}>Create ChatRoom</button>
        </div>
    );
};

export default CreateRoom;
